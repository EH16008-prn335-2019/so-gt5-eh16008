#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "string.h"

int main() {

  int fd, salida;
  char buffer[50];
  ssize_t tamanioMensaje;
  char mensaje[] = "Sistemas Operativos 2020 - esto es un txt \nmostrando contenido de archivo txt, con llamadas al sistema :D\n";
  tamanioMensaje = strlen(mensaje);
  char ruta[] = "file.txt";
  fd = open(ruta, O_RDWR|O_CREAT,S_IRUSR|S_IWUSR);
  salida = write(fd,mensaje,tamanioMensaje);
  close(fd);

  int pid, status;
  char *argumentos[] = {"cat", "-A", "file.txt"};
  char *env[] = {"TERM=xterm"};
  pid = fork();
  if (pid == -1) {
    printf("Error en fork\n");
    exit(-1);
  }else if (pid == 0) {
    execve("/usr/bin/cat", argumentos, env);
    perror("Error de exec, en la llamada al sistema");
    exit(-1);
  }else{
    while (wait(&status) != pid);
    (status == 0)?printf("\nFuncion de comando cat Exitosa!\n"):printf("Error en el hijo\n");
  }

  return 0;
}
