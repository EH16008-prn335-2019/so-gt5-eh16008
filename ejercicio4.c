#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main()
{
  char buf[30];
  int i = 0;
  ssize_t inread;
  write( 1, "Ingrese Texto (MAX 30 Bytes): ", 30);
  inread = read(0, buf, sizeof buf -1);
  buf[inread-1] = '\0';
  while (i < 3)
  {
    write( 1, buf, strlen(buf));
    write( 1, "\n", 1);
    i++;
  }
}
